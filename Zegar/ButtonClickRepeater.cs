﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

// http://stackoverflow.com/questions/12797351/long-pressed-button

namespace Zegar
{
    class ButtonClickRepeater
    {
        public event EventHandler Click;
        private readonly ClockForm form;

        private Button button;
        private Timer timer;

        // http://stackoverflow.com/questions/5646954/how-to-access-winform-textbox-control-from-another-class
        public ButtonClickRepeater(Button button, int interval, ClockForm form)
        {
            this.form = form;
            if (button == null) throw new ArgumentNullException();

            this.button = button;
            button.MouseDown += new MouseEventHandler(button_MouseDown);
            button.MouseUp += new MouseEventHandler(button_MouseUp);
            button.Disposed += new EventHandler(button_Disposed);

            timer = new Timer();
            timer.Interval = interval;
            timer.Tick += new EventHandler(timer_Tick);
        }

        void button_MouseDown(object sender, MouseEventArgs e)
        {
            OnClick(EventArgs.Empty);
            timer.Start();
        }

        void button_MouseUp(object sender, MouseEventArgs e)
        {
            timer.Stop();
        }

        void button_Disposed(object sender, EventArgs e)
        {
            timer.Stop();
            timer.Dispose();
        }

        void timer_Tick(object sender, EventArgs e)
        {
            if(button.Name == "button1")
            {
                form.button1_Tick();
            }
            else if(button.Name == "buttonB") // brzydkie, ale nie zmienia sie tak czesto nazw obiektow
            {
                ClockFunctions.SettingsMode = true;
                if (ClockFunctions.CurrentDisplay == ClockFunctions.DisplayStatus.StandardTime)
                {
                    ClockFunctions.CurrentTimeChange = 0;
                }
                else if(ClockFunctions.CurrentDisplay == ClockFunctions.DisplayStatus.Date)
                {
                    ClockFunctions.CurrentDateChange = 0;
                }
                else if (ClockFunctions.CurrentDisplay == ClockFunctions.DisplayStatus.T2)
                {
                    ClockFunctions.CurrentT2Change = 0;
                }
                else if (ClockFunctions.CurrentDisplay == ClockFunctions.DisplayStatus.Alarm)
                {
                    ClockFunctions.CurrentAlarmChange = 0;
                }
                form.labelCurrentModeEdit = ClockFunctions.CurrentDisplay.ToString() + " SETTINGS MODE";
                form.label2Text = "B wciśnięte!"; // dla testów
            }
            
            OnClick(EventArgs.Empty);
        }

        protected void OnClick(EventArgs e)
        {
            if (Click != null)
            {
                Click(button, e);
                //button.Text = "test";
            }
                
        }
    }
}
