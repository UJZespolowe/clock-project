﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Zegar
{
    public interface AlarmStatus
    {
        int AlarmOn { get; set; }
        int AlarmOff { get; set; }
        int SignalOn { get; set; }
        int SignalOff { get; set; }
        DisplayFormat DisplayFormat { get; set; }
    }
}