﻿namespace Zegar
{
    partial class ClockForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.panelClock = new System.Windows.Forms.Panel();
            this.digitalClock12HourMode = new System.Windows.Forms.RichTextBox();
            this.digitalClockSeconds = new System.Windows.Forms.RichTextBox();
            this.digitalClockMinutes = new System.Windows.Forms.RichTextBox();
            this.clockDot = new System.Windows.Forms.Label();
            this.digitalClockHours = new System.Windows.Forms.RichTextBox();
            this.buttonA = new System.Windows.Forms.Button();
            this.buttonB = new System.Windows.Forms.Button();
            this.labelCurrentMode = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.panelMainArea = new System.Windows.Forms.Panel();
            this.panelDate = new System.Windows.Forms.Panel();
            this.dateYear = new System.Windows.Forms.RichTextBox();
            this.dateSeperator2 = new System.Windows.Forms.Label();
            this.dateDay = new System.Windows.Forms.RichTextBox();
            this.dateSeperator1 = new System.Windows.Forms.Label();
            this.dateMonth = new System.Windows.Forms.RichTextBox();
            this.timerClockMinutes = new System.Windows.Forms.Timer(this.components);
            this.timerClockHours = new System.Windows.Forms.Timer(this.components);
            this.panelClock.SuspendLayout();
            this.panelMainArea.SuspendLayout();
            this.panelDate.SuspendLayout();
            this.SuspendLayout();
            // 
            // panelClock
            // 
            this.panelClock.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panelClock.Controls.Add(this.digitalClock12HourMode);
            this.panelClock.Controls.Add(this.digitalClockSeconds);
            this.panelClock.Controls.Add(this.digitalClockMinutes);
            this.panelClock.Controls.Add(this.clockDot);
            this.panelClock.Controls.Add(this.digitalClockHours);
            this.panelClock.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelClock.Location = new System.Drawing.Point(0, 0);
            this.panelClock.Name = "panelClock";
            this.panelClock.Size = new System.Drawing.Size(470, 156);
            this.panelClock.TabIndex = 0;
            this.panelClock.Paint += new System.Windows.Forms.PaintEventHandler(this.panelClock_Paint);
            // 
            // digitalClock12HourMode
            // 
            this.digitalClock12HourMode.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.digitalClock12HourMode.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.digitalClock12HourMode.Enabled = false;
            this.digitalClock12HourMode.Font = new System.Drawing.Font("Microsoft Sans Serif", 40F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.digitalClock12HourMode.Location = new System.Drawing.Point(341, 88);
            this.digitalClock12HourMode.Multiline = false;
            this.digitalClock12HourMode.Name = "digitalClock12HourMode";
            this.digitalClock12HourMode.Size = new System.Drawing.Size(125, 64);
            this.digitalClock12HourMode.TabIndex = 4;
            this.digitalClock12HourMode.Text = "";
            // 
            // digitalClockSeconds
            // 
            this.digitalClockSeconds.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.digitalClockSeconds.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.digitalClockSeconds.Dock = System.Windows.Forms.DockStyle.Top;
            this.digitalClockSeconds.Enabled = false;
            this.digitalClockSeconds.Font = new System.Drawing.Font("Microsoft Sans Serif", 72F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.digitalClockSeconds.Location = new System.Drawing.Point(341, 0);
            this.digitalClockSeconds.Multiline = false;
            this.digitalClockSeconds.Name = "digitalClockSeconds";
            this.digitalClockSeconds.Size = new System.Drawing.Size(125, 88);
            this.digitalClockSeconds.TabIndex = 3;
            this.digitalClockSeconds.Text = "";
            // 
            // digitalClockMinutes
            // 
            this.digitalClockMinutes.BackColor = System.Drawing.SystemColors.Window;
            this.digitalClockMinutes.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.digitalClockMinutes.Dock = System.Windows.Forms.DockStyle.Left;
            this.digitalClockMinutes.Enabled = false;
            this.digitalClockMinutes.Font = new System.Drawing.Font("Microsoft Sans Serif", 95F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.digitalClockMinutes.Location = new System.Drawing.Point(200, 0);
            this.digitalClockMinutes.Multiline = false;
            this.digitalClockMinutes.Name = "digitalClockMinutes";
            this.digitalClockMinutes.Size = new System.Drawing.Size(141, 152);
            this.digitalClockMinutes.TabIndex = 2;
            this.digitalClockMinutes.Text = "";
            // 
            // clockDot
            // 
            this.clockDot.BackColor = System.Drawing.SystemColors.Control;
            this.clockDot.Dock = System.Windows.Forms.DockStyle.Left;
            this.clockDot.Font = new System.Drawing.Font("Microsoft Sans Serif", 69.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.clockDot.ForeColor = System.Drawing.SystemColors.GrayText;
            this.clockDot.Location = new System.Drawing.Point(141, 0);
            this.clockDot.Name = "clockDot";
            this.clockDot.Size = new System.Drawing.Size(59, 152);
            this.clockDot.TabIndex = 1;
            this.clockDot.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // digitalClockHours
            // 
            this.digitalClockHours.BackColor = System.Drawing.SystemColors.Window;
            this.digitalClockHours.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.digitalClockHours.Dock = System.Windows.Forms.DockStyle.Left;
            this.digitalClockHours.Enabled = false;
            this.digitalClockHours.Font = new System.Drawing.Font("Microsoft Sans Serif", 95F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.digitalClockHours.Location = new System.Drawing.Point(0, 0);
            this.digitalClockHours.Multiline = false;
            this.digitalClockHours.Name = "digitalClockHours";
            this.digitalClockHours.Size = new System.Drawing.Size(141, 152);
            this.digitalClockHours.TabIndex = 0;
            this.digitalClockHours.Text = "";
            // 
            // buttonA
            // 
            this.buttonA.Location = new System.Drawing.Point(28, 208);
            this.buttonA.Name = "buttonA";
            this.buttonA.Size = new System.Drawing.Size(113, 54);
            this.buttonA.TabIndex = 1;
            this.buttonA.Text = "A";
            this.buttonA.UseVisualStyleBackColor = true;
            this.buttonA.Click += new System.EventHandler(this.buttonA_Click);
            // 
            // buttonB
            // 
            this.buttonB.Location = new System.Drawing.Point(232, 208);
            this.buttonB.Name = "buttonB";
            this.buttonB.Size = new System.Drawing.Size(98, 53);
            this.buttonB.TabIndex = 2;
            this.buttonB.Text = "B";
            this.buttonB.UseVisualStyleBackColor = true;
            this.buttonB.Click += new System.EventHandler(this.buttonB_Click);
            // 
            // labelCurrentMode
            // 
            this.labelCurrentMode.Location = new System.Drawing.Point(370, 208);
            this.labelCurrentMode.Name = "labelCurrentMode";
            this.labelCurrentMode.Size = new System.Drawing.Size(113, 37);
            this.labelCurrentMode.TabIndex = 3;
            this.labelCurrentMode.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(28, 8);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(139, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "Dzień dobry jestem zegarem";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(28, 279);
            this.button1.Margin = new System.Windows.Forms.Padding(2);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(113, 19);
            this.button1.TabIndex = 5;
            this.button1.Text = "Wcisnij i przytrzymaj!";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Visible = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            this.button1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.button1_MouseDown);
            this.button1.MouseLeave += new System.EventHandler(this.button1_MouseLeave);
            this.button1.MouseUp += new System.Windows.Forms.MouseEventHandler(this.button1_MouseUp);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.ForeColor = System.Drawing.Color.Black;
            this.label2.Location = new System.Drawing.Point(220, 279);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(23, 13);
            this.label2.TabIndex = 6;
            this.label2.Text = "Nic";
            this.label2.Visible = false;
            this.label2.Click += new System.EventHandler(this.label2_Click);
            // 
            // panelMainArea
            // 
            this.panelMainArea.Controls.Add(this.panelClock);
            this.panelMainArea.Controls.Add(this.panelDate);
            this.panelMainArea.Location = new System.Drawing.Point(30, 37);
            this.panelMainArea.Name = "panelMainArea";
            this.panelMainArea.Size = new System.Drawing.Size(470, 156);
            this.panelMainArea.TabIndex = 7;
            // 
            // panelDate
            // 
            this.panelDate.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panelDate.Controls.Add(this.dateYear);
            this.panelDate.Controls.Add(this.dateSeperator2);
            this.panelDate.Controls.Add(this.dateDay);
            this.panelDate.Controls.Add(this.dateSeperator1);
            this.panelDate.Controls.Add(this.dateMonth);
            this.panelDate.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelDate.Location = new System.Drawing.Point(0, 0);
            this.panelDate.Name = "panelDate";
            this.panelDate.Size = new System.Drawing.Size(470, 156);
            this.panelDate.TabIndex = 8;
            this.panelDate.Visible = false;
            // 
            // dateYear
            // 
            this.dateYear.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dateYear.Dock = System.Windows.Forms.DockStyle.Left;
            this.dateYear.Enabled = false;
            this.dateYear.Font = new System.Drawing.Font("Microsoft Sans Serif", 50F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)), true);
            this.dateYear.Location = new System.Drawing.Point(299, 0);
            this.dateYear.Name = "dateYear";
            this.dateYear.Size = new System.Drawing.Size(175, 152);
            this.dateYear.TabIndex = 9;
            this.dateYear.Text = "";
            // 
            // dateSeperator2
            // 
            this.dateSeperator2.Dock = System.Windows.Forms.DockStyle.Left;
            this.dateSeperator2.Font = new System.Drawing.Font("Microsoft Sans Serif", 50F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.dateSeperator2.ForeColor = System.Drawing.SystemColors.GrayText;
            this.dateSeperator2.Location = new System.Drawing.Point(238, 0);
            this.dateSeperator2.Name = "dateSeperator2";
            this.dateSeperator2.Size = new System.Drawing.Size(61, 152);
            this.dateSeperator2.TabIndex = 8;
            this.dateSeperator2.Text = "-";
            this.dateSeperator2.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // dateDay
            // 
            this.dateDay.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dateDay.Dock = System.Windows.Forms.DockStyle.Left;
            this.dateDay.Enabled = false;
            this.dateDay.Font = new System.Drawing.Font("Microsoft Sans Serif", 50F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)), true);
            this.dateDay.Location = new System.Drawing.Point(147, 0);
            this.dateDay.Name = "dateDay";
            this.dateDay.Size = new System.Drawing.Size(91, 152);
            this.dateDay.TabIndex = 7;
            this.dateDay.Text = "";
            // 
            // dateSeperator1
            // 
            this.dateSeperator1.Dock = System.Windows.Forms.DockStyle.Left;
            this.dateSeperator1.Font = new System.Drawing.Font("Microsoft Sans Serif", 50F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.dateSeperator1.ForeColor = System.Drawing.SystemColors.GrayText;
            this.dateSeperator1.Location = new System.Drawing.Point(86, 0);
            this.dateSeperator1.Name = "dateSeperator1";
            this.dateSeperator1.Size = new System.Drawing.Size(61, 152);
            this.dateSeperator1.TabIndex = 6;
            this.dateSeperator1.Text = "-";
            this.dateSeperator1.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // dateMonth
            // 
            this.dateMonth.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dateMonth.Dock = System.Windows.Forms.DockStyle.Left;
            this.dateMonth.Enabled = false;
            this.dateMonth.Font = new System.Drawing.Font("Microsoft Sans Serif", 50F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)), true);
            this.dateMonth.Location = new System.Drawing.Point(0, 0);
            this.dateMonth.Multiline = false;
            this.dateMonth.Name = "dateMonth";
            this.dateMonth.Size = new System.Drawing.Size(86, 152);
            this.dateMonth.TabIndex = 5;
            this.dateMonth.Text = "";
            // 
            // timerClockMinutes
            // 
            this.timerClockMinutes.Interval = 60000;
            this.timerClockMinutes.Tick += new System.EventHandler(this.timerClockMinutes_Tick);
            // 
            // timerClockHours
            // 
            this.timerClockHours.Interval = 3600000;
            this.timerClockHours.Tick += new System.EventHandler(this.timerClockHours_Tick);
            // 
            // ClockForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(512, 307);
            this.Controls.Add(this.panelMainArea);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.labelCurrentMode);
            this.Controls.Add(this.buttonB);
            this.Controls.Add(this.buttonA);
            this.Name = "ClockForm";
            this.Text = "Clock";
            this.Load += new System.EventHandler(this.ClockForm_Load);
            this.panelClock.ResumeLayout(false);
            this.panelMainArea.ResumeLayout(false);
            this.panelDate.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panelClock;
        private System.Windows.Forms.Button buttonA;
        private System.Windows.Forms.Button buttonB;
        private System.Windows.Forms.Label labelCurrentMode;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button button1;
        public System.Windows.Forms.Label label2;
        private System.Windows.Forms.RichTextBox digitalClockHours;
        private System.Windows.Forms.Label clockDot;
        private System.Windows.Forms.Panel panelMainArea;
        private System.Windows.Forms.RichTextBox digitalClockMinutes;
        private System.Windows.Forms.Timer timerClockMinutes;
        private System.Windows.Forms.RichTextBox digitalClockSeconds;
        private System.Windows.Forms.Panel panelDate;
        private System.Windows.Forms.RichTextBox dateYear;
        private System.Windows.Forms.Label dateSeperator2;
        private System.Windows.Forms.RichTextBox dateDay;
        private System.Windows.Forms.Label dateSeperator1;
        private System.Windows.Forms.RichTextBox dateMonth;
        private System.Windows.Forms.RichTextBox digitalClock12HourMode;
        private System.Windows.Forms.Timer timerClockHours;
    }
}

