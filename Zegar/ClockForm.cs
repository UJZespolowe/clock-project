﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Media;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using System.Windows.Forms;

namespace Zegar
{
    public partial class ClockForm : Form
    {
        private ClockFunctions _clockFunctions = new ClockFunctions();

        //private DateTime _currentTime = ClockFunctions.StandardTime;

        public ClockForm()
        {
            InitializeComponent();
        }

        internal ClockFunctions ClockFunctions
        {
            get
            {
                return _clockFunctions;
            }

        }

        /*internal DateTime CurrentDateTime
        {
            get
            {
                return _currentTime;
            }
            set
            {
                _currentTime = value;
            }
        }*/

        private void ClockForm_Load(object sender, EventArgs e)
        {

            // this.timerClockHours.Start();
            // this.timerClockMinutes.Start();
            ButtonClickRepeater repeater1 = new ButtonClickRepeater(this.button1, 1000, this);
            repeater1.Click += new EventHandler(repeater1_Click);

            ButtonClickRepeater repeaterB = new ButtonClickRepeater(this.buttonB, 1000, this);
            repeaterB.Click += new EventHandler(repeaterB_Click);

            ClockFunctions.CurrentAlarmSetting = TimeSpan.Zero;
            ClockFunctions.Alarm.Elapsed += new ElapsedEventHandler(ClockFunctions.OnAlarm);
            ClockFunctions.Alarm.Interval = 1000;
            ClockFunctions.Alarm.Enabled = true;

            ClockFunctions.CurrentDisplay = ClockFunctions.DisplayStatus.End; // brzydki trick żeby w buttonA_Click przeskoczyło na StandardTime.
            buttonA_Click(sender, e); // wywołujemy tryb zegara
        }



        /*private void RepeaterB_Click(object sender, EventArgs e)
        {
            buttonB_Click(sender, e);
        }*/

        private void buttonB_Click(object sender, EventArgs e)
        {
            if(ClockFunctions.SettingsMode)
            {
                
                if (ClockFunctions.CurrentDisplay == ClockFunctions.DisplayStatus.StandardTime)
                {
                    if (ClockFunctions.CurrentTimeChange.ToString() == "Mode") // bardzo brzydkie i tymczasowe
                    {
                        ClockFunctions.SettingsMode = false;
                        labelCurrentMode.Text = ClockFunctions.CurrentDisplay.ToString();
                    }
                    else
                    {
                        ClockFunctions.CurrentTimeChange++;
                    }
                    return;
                }
                else if(ClockFunctions.CurrentDisplay == ClockFunctions.DisplayStatus.Date)
                {
                    if (ClockFunctions.CurrentDateChange.ToString() == "Months") // bardzo brzydkie i tymczasowe
                    {
                        ClockFunctions.SettingsMode = false;
                        labelCurrentMode.Text = ClockFunctions.CurrentDisplay.ToString();
                    }
                    else
                    {
                        ClockFunctions.CurrentDateChange++;
                    }
                    return;
                }
                else if(ClockFunctions.CurrentDisplay == ClockFunctions.DisplayStatus.T2)
                {
                    if (ClockFunctions.CurrentT2Change.ToString() == "Hours")
                    {
                        ClockFunctions.SettingsMode = false;
                        labelCurrentMode.Text = ClockFunctions.CurrentDisplay.ToString();
                    }
                    else
                    {
                        ClockFunctions.CurrentT2Change++;
                    }
                    return;
                }
                else if(ClockFunctions.CurrentDisplay == ClockFunctions.DisplayStatus.Stoper)
                {
                    ClockFunctions.Stoper.Reset();
                    ClockFunctions.SettingsMode = false;
                    labelCurrentMode.Text = ClockFunctions.CurrentDisplay.ToString();
                    return;
                }
                else if(ClockFunctions.CurrentDisplay == ClockFunctions.DisplayStatus.Alarm)
                {
                    if (ClockFunctions.CurrentAlarmChange.ToString() == "OnOff")
                    {
                        ClockFunctions.SettingsMode = false;
                        labelCurrentMode.Text = ClockFunctions.CurrentDisplay.ToString();
                    }
                    else
                    {
                        ClockFunctions.CurrentAlarmChange++;
                    }
                    return;
                }
            } // end settings mode
            else
            {
                if (ClockFunctions.CurrentDisplay == ClockFunctions.DisplayStatus.Stoper)
                {
                    if (ClockFunctions.Stoper.IsRunning)
                    {
                        ClockFunctions.Stoper.Stop();
                        SystemSounds.Beep.Play();
                    }
                    else
                    {
                        ClockFunctions.Stoper.Start();
                        SystemSounds.Beep.Play();

                    }
                }
            }
            //ClockFunctions.SettingsMode = true;
            return;
            ClockFunctions.ChangeSettings();
           
        }

        private async void buttonA_Click(object sender, EventArgs e)
        {

            if (ClockFunctions.SettingsMode)
            {
                labelCurrentMode.Text = ClockFunctions.CurrentDisplay.ToString() + " SETTINGS MODE / A";
                if (ClockFunctions.CurrentDisplay == ClockFunctions.DisplayStatus.StandardTime)
                {
                    if (ClockFunctions.CurrentTimeChange.ToString() == "Seconds")
                    {
                        ClockFunctions.CurrentTimeSetting = ClockFunctions.CurrentTimeSetting.Add(TimeSpan.FromSeconds(1));
                        if (ClockFunctions.CurrentTimeSetting.Seconds == 59)
                        {
                            ClockFunctions.CurrentTimeSetting = TimeSpan.FromSeconds(-1);
                        }
                    }
                    else if (ClockFunctions.CurrentTimeChange.ToString() == "Minutes")
                    {
                        ClockFunctions.CurrentTimeSetting = ClockFunctions.CurrentTimeSetting.Add(TimeSpan.FromMinutes(1));
                        if (ClockFunctions.CurrentTimeSetting.Minutes == 59)
                        {
                            ClockFunctions.CurrentTimeSetting = TimeSpan.FromMinutes(-1);
                        }
                    }
                    else if (ClockFunctions.CurrentTimeChange.ToString() == "Hours")
                    {
                        ClockFunctions.CurrentTimeSetting = ClockFunctions.CurrentTimeSetting.Add(TimeSpan.FromHours(1));
                        if (ClockFunctions.CurrentTimeSetting.Hours == 23)
                        {
                            ClockFunctions.CurrentTimeSetting = TimeSpan.FromHours(-1);
                        }
                    }
                    else if (ClockFunctions.CurrentTimeChange.ToString() == "Mode")
                    {
                        ClockFunctions.CurrentTimeMode24h = !ClockFunctions.CurrentTimeMode24h;
                        

                    }
                    this.digitalClockHours.Text = ClockFunctions.StandardTime.Hour.ToString("D2"); // Hours;
                    this.digitalClockMinutes.Text = ClockFunctions.StandardTime.Minute.ToString("D2"); // Minutes;
                    this.digitalClockSeconds.Text = ClockFunctions.StandardTime.Second.ToString("D2"); // Seconds;
                    if (ClockFunctions.CurrentTimeMode24h)
                    {
                        // this.digitalClock12HourMode.Text = "21"; // dla testów
                        
                    }
                    else
                    {
                        // this.digitalClock12HourMode.Text = "12";
                    }
                }
                else if(ClockFunctions.CurrentDisplay == ClockFunctions.DisplayStatus.Date)
                {
                    if(ClockFunctions.CurrentDateChange.ToString() == "Days")
                    {
                        ClockFunctions.CurrentDateSetting = ClockFunctions.CurrentDateSetting.Add(TimeSpan.FromDays(1));
                        /*if (ClockFunctions.CurrentDateSetting.Days == 30) // ?????
                        {
                            ClockFunctions.CurrentDateSetting = TimeSpan.FromDays(-1);
                        }*/
                    }
                    else if (ClockFunctions.CurrentDateChange.ToString() == "Months")
                    {
                        DateTime now = DateTime.Now;
                        TimeSpan temp = now.AddMonths(1) - now;
                        // MessageBox.Show(ClockFunctions.CurrentTimeSetting.Days.ToString());
                        ClockFunctions.CurrentDateSetting = ClockFunctions.CurrentDateSetting.Add(temp); // nie dziala to zbyt dobrze.
                        /*if (ClockFunctions.CurrentDateSetting. == 11)
                        {
                            ClockFunctions.CurrentDateSetting = TimeSpan.FromMonths(-1);
                        }*/
                    }
                    this.dateDay.Text = ClockFunctions.Date.Day.ToString("D2");
                    this.dateMonth.Text = ClockFunctions.Date.Month.ToString("D2");
                    this.dateYear.Text = ClockFunctions.Date.Year.ToString("D2");
                }
                else if(ClockFunctions.CurrentDisplay == ClockFunctions.DisplayStatus.T2)
                {
                    if (ClockFunctions.CurrentT2Change.ToString() == "Minutes")
                    {
                        ClockFunctions.CurrentT2Setting = ClockFunctions.CurrentT2Setting.Add(TimeSpan.FromMinutes(1));
                        if (ClockFunctions.CurrentT2Setting.Minutes == 59)
                        {
                            ClockFunctions.CurrentT2Setting = TimeSpan.FromMinutes(-1);
                        }
                    }
                    else if (ClockFunctions.CurrentT2Change.ToString() == "Hours")
                    {
                        ClockFunctions.CurrentT2Setting = ClockFunctions.CurrentT2Setting.Add(TimeSpan.FromHours(1));
                        if (ClockFunctions.CurrentT2Setting.Hours == 23)
                        {
                            ClockFunctions.CurrentT2Setting = TimeSpan.FromHours(-1);
                        }
                    }
                    this.digitalClockHours.Text = ClockFunctions.T2.Hour.ToString("D2"); // Hours;
                    this.digitalClockMinutes.Text = ClockFunctions.T2.Minute.ToString("D2"); // Minutes;
                    this.digitalClockSeconds.Text = ClockFunctions.T2.Second.ToString("D2"); // Seconds;
                }
                else if (ClockFunctions.CurrentDisplay == ClockFunctions.DisplayStatus.Alarm)
                {
                    if (ClockFunctions.CurrentAlarmChange.ToString() == "Minutes")
                    {
                        ClockFunctions.CurrentAlarmSetting = ClockFunctions.CurrentAlarmSetting.Add(TimeSpan.FromMinutes(1));
                        if (ClockFunctions.CurrentAlarmSetting.Minutes == 59)
                        {
                            ClockFunctions.CurrentAlarmSetting = TimeSpan.FromMinutes(-1);
                        }
                    }
                    else if (ClockFunctions.CurrentAlarmChange.ToString() == "Hours")
                    {
                        ClockFunctions.CurrentAlarmSetting = ClockFunctions.CurrentAlarmSetting.Add(TimeSpan.FromHours(1));
                        if (ClockFunctions.CurrentAlarmSetting.Hours == 23)
                        {
                            ClockFunctions.CurrentAlarmSetting = TimeSpan.FromHours(-1);
                        }
                    }
                    else if (ClockFunctions.CurrentAlarmChange.ToString() == "OnOff")
                    {
                        if (ClockFunctions.Alarm.Enabled)
                        {
                            ClockFunctions.Alarm.Stop();
                            this.digitalClock12HourMode.Text = "OFF";
                        }
                        else
                        {
                            ClockFunctions.Alarm.Start();
                            this.digitalClock12HourMode.Text = "ON";
                        }
                    }
                    this.digitalClockHours.Text = ClockFunctions.CurrentAlarmSetting.Hours.ToString("D2");
                    this.digitalClockMinutes.Text = ClockFunctions.CurrentAlarmSetting.Minutes.ToString("D2");

                    // ClockFunctions.Alarm.Interval = ClockFunctions.CurrentAlarmSetting.TotalMilliseconds;
                }

            } // end settings mode
            else
            {
                // TUTAJ ZMIANA ENUMA
                SystemSounds.Asterisk.Play();
                if (ClockFunctions.CurrentDisplay.ToString() == "End") // bardzo brzydkie i tymczasowe
                {
                    ClockFunctions.CurrentDisplay = 0; // jeśli "End", wracamy do początku czyli do zegara
                    
                }
                else
                {
                    ClockFunctions.CurrentDisplay++;
                    if (ClockFunctions.CurrentDisplay.ToString() == "End")
                    {
                        ClockFunctions.CurrentDisplay = 0;

                    }
                }

                labelCurrentMode.Text = ClockFunctions.CurrentDisplay.ToString();
                if (ClockFunctions.CurrentDisplay == ClockFunctions.DisplayStatus.StandardTime)
                {
                    while (ClockFunctions.CurrentDisplay == ClockFunctions.DisplayStatus.StandardTime)
                    {
                        this.panelClock.Visible = true;
                        this.panelDate.Visible = false;
                        //CurrentDateTime = ClockFunctions.StandardTime;
                        clockDot.Text = ":";
                        this.digitalClockHours.Text = ClockFunctions.StandardTime.Hour.ToString("D2"); // Hours;
                        this.digitalClockMinutes.Text = ClockFunctions.StandardTime.Minute.ToString("D2"); // Minutes;
                        this.digitalClockSeconds.Text = ClockFunctions.StandardTime.Second.ToString("D2"); // Seconds;
                        if (ClockFunctions.CurrentTimeMode24h)
                        {
                            this.digitalClock12HourMode.Text = "24".ToString();
                            
                        }
                        else
                        {
                            //MessageBox.Show(ClockFunctions.CurrentTimeMode24h.ToString());
                            if (ClockFunctions.StandardTime.Hour >= 0 && ClockFunctions.StandardTime.Hour < 12 && ClockFunctions.CurrentTimeModeAM)
                            {
                                this.digitalClock12HourMode.Text = "AM";
                            }
                            else
                            {
                                this.digitalClock12HourMode.Text = "PM";
                            }
                        }
                        
                        await Task.Delay(1000); // odświeżanie zegara co sekundę
                    }
                    this.digitalClock12HourMode.Text = ""; // czyścimy po wyjściu z trybu StandardTime
                    // return;
                }
                else if(ClockFunctions.CurrentDisplay == ClockFunctions.DisplayStatus.Stoper)
                {
                    this.panelClock.Visible = true;
                    this.panelDate.Visible = false;
                    clockDot.Text = ":";
                    while (ClockFunctions.CurrentDisplay == ClockFunctions.DisplayStatus.Stoper)
                    {
                            TimeSpan stoperTime = ClockFunctions.Stoper.Elapsed;
                            this.digitalClockHours.Text = stoperTime.Minutes.ToString("D2");
                            this.digitalClockMinutes.Text = stoperTime.Seconds.ToString("D2");
                            this.digitalClockSeconds.Text = stoperTime.Milliseconds.ToString("D2"); // z jakiegoś powodu nie formatuje się to prawidłowo - pokazuje 3 cyfry
                            await Task.Delay(100);
                    }

                }
                else if(ClockFunctions.CurrentDisplay == ClockFunctions.DisplayStatus.Date)
                {
                    //clockDot.Text = "";
                    this.panelClock.Visible = false;
                    this.panelDate.Visible = true;
                    //CurrentDateTime = ClockFunctions.StandardTime;
                    this.dateDay.Text = ClockFunctions.Date.Day.ToString("D2");
                    this.dateMonth.Text = ClockFunctions.Date.Month.ToString("D2");
                    this.dateYear.Text = ClockFunctions.Date.Year.ToString("D2");
                }
                else if (ClockFunctions.CurrentDisplay == ClockFunctions.DisplayStatus.T2)
                {
                    this.digitalClock12HourMode.Text = "";
                    while (ClockFunctions.CurrentDisplay == ClockFunctions.DisplayStatus.T2)
                    {
                        this.panelClock.Visible = true;
                        this.panelDate.Visible = false;
                        //CurrentDateTime = ClockFunctions.StandardTime;
                        clockDot.Text = ":";
                        this.digitalClockHours.Text = ClockFunctions.T2.Hour.ToString("D2"); // Hours;
                        this.digitalClockMinutes.Text = ClockFunctions.T2.Minute.ToString("D2"); // Minutes;
                        this.digitalClockSeconds.Text = ClockFunctions.T2.Second.ToString("D2"); // Seconds;
                        await Task.Delay(1000); // odświeżanie zegara co sekundę
                    }
                }
                else if (ClockFunctions.CurrentDisplay == ClockFunctions.DisplayStatus.Alarm)
                {
                    this.panelClock.Visible = true;
                    this.panelDate.Visible = false;
                    clockDot.Text = ":";
                    this.digitalClockHours.Text = ClockFunctions.CurrentAlarmSetting.Hours.ToString("D2");
                    this.digitalClockMinutes.Text = ClockFunctions.CurrentAlarmSetting.Minutes.ToString("D2");
                    if (ClockFunctions.Alarm.Enabled)
                    {
                        this.digitalClock12HourMode.Text = "ON";
                    }
                    else
                    {
                        this.digitalClock12HourMode.Text = "OFF";
                    }
                    this.digitalClockSeconds.Text = "";

                    /* while (ClockFunctions.Alarm.Enabled)
                    {
                        // ClockFunctions.Alarm.
                    }*/

                }
                //digitalClockHours.Text = ClockFunctions.DisplayFormat.Hours.ToString();
                //digitalClockMinutes.Text = ClockFunctions.DisplayFormat.Minutes.ToString();

            }
        }

        private async void button1_Click(object sender, EventArgs e)
        {
            
            label2.Text = "button1 Click";
        }

        public void button1_Tick() // dla testów. wywoływane w ButtonClickRepeater
        {
            button1.Text = "Test wcisniecia";
            label2.Text = "Brawo! Przycisk wciśnięty!";
        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void repeater1_Click(object sender, EventArgs e)
        {    
            //label2.Text = "Test klik";
            //throw new System.NotImplementedException();
        }

        private void repeaterB_Click(object sender, EventArgs e)
        {
            //throw new NotImplementedException();
        }

        private void button1_MouseDown(object sender, MouseEventArgs e)
        {
            
            label2.Text = "Lewy wcisniety";
        }

        private void button1_MouseUp(object sender, MouseEventArgs e)
        {
            button1.Text = "Wcisnij mnie";
            label2.Text = "wciskaj dalej!";
        }

        private void button1_MouseLeave(object sender, EventArgs e)
        {
            button1.Text = "Wcisnij mnie!";
            
        }

        public string label2Text // tylko dla testow
        {
            get { return label2.Text; }
            set { label2.Text = value; }
        }

        public string labelCurrentModeEdit // tylko dla testow
        {
            get { return labelCurrentMode.Text; }
            set { labelCurrentMode.Text = value; }
        }

        private void timerClockMinutes_Tick(object sender, EventArgs e)
        {
            //CurrentDateTime = CurrentDateTime.AddMinutes(1);
            //digitalClockMinutes.Text = CurrentDateTime.Minute.ToString();
        }

        private void timerClockHours_Tick(object sender, EventArgs e)
        {
            //CurrentDateTime = CurrentDateTime.AddHours(1);
            //digitalClockHours.Text = CurrentDateTime.Hour.ToString();
        }

        private void panelClock_Paint(object sender, PaintEventArgs e)
        {

        }
    }
}
