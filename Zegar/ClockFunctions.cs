﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;
using System.Timers;
using System.Media;
using System.Windows.Forms;

namespace Zegar
{
    internal class ClockFunctions
    {

        // docelowo zamienić enuma klasą z komparatorem
        // http://stackoverflow.com/questions/430264/next-or-previous-enum
        public enum DisplayStatus // aktualnie wyświetlany moduł
        {
            StandardTime,
            Date,
            Stoper,
            Alarm,
            T2,
            End // zaślepka
        }

        public enum TimeChange // co aktualnie zmieniamy
        {
            Start,
            Seconds,
            Minutes,
            Hours,
            Mode
        }

        public static TimeChange CurrentTimeChange;

        public enum DateChange
        {
            Start,
            Days,
            Months
        }

        public static DateChange CurrentDateChange;

        public enum T2Change
        {
            Start,
            Minutes,
            Hours,
            Mode
        }

        private static Stopwatch stopwatch = new Stopwatch();

        private static System.Timers.Timer alarmTimer = new System.Timers.Timer();

        public static T2Change CurrentT2Change;

        public static TimeSpan CurrentTimeSetting // aktualne ustawienie czasu
        {
            get; set;
        }

        public static bool CurrentTimeMode24h = true;

        public static bool CurrentTimeModeAM;

        public static TimeSpan CurrentDateSetting
        {
            get; set;
        }

        public static TimeSpan CurrentT2Setting
        {
            get; set;
        }


        public static DisplayStatus CurrentDisplay;

        public static bool SettingsMode = false;

        public enum AlarmChange
        {
            Start,
            Minutes,
            Hours,
            OnOff
        }

        public static AlarmChange CurrentAlarmChange;

        public System.Timers.Timer Alarm
        {
            get
            {
                return alarmTimer;
            }

            set
            {
            }
        }

        public TimeSpan CurrentAlarmSetting
        {
            get; set;
        }

        public void OnAlarm(Object source, ElapsedEventArgs e)
        {
            /*DateTime dt = DateTime.Now;
            TimeSpan ts = TimeSpan.FromHours(23) + TimeSpan.FromMinutes(56);
            if (dt.Hour == ts.Hours && dt.Minute == ts.Minutes)
            {
                MessageBox.Show("DUPA");
            }
            else
            {
                MessageBox.Show(dt.Hour + ":" + dt.Minute + ":" + dt.Second + 
                    " / " + ts.Hours + ":" + ts.Minutes + ":" + ts.Seconds);
            }
            */


            try
            {
                //Sound Alarm
                //if (this.StandardTime.Equals(this.CurrentAlarmSetting))
                if (StandardTime.Hour == CurrentAlarmSetting.Hours && StandardTime.Minute == CurrentAlarmSetting.Minutes)
                {
                    //Dzwiękowy koszmar :)
                    SystemSounds.Beep.Play();
                    SystemSounds.Beep.Play();
                    SystemSounds.Beep.Play();
                    SystemSounds.Beep.Play();
                    SystemSounds.Beep.Play();
                    SystemSounds.Beep.Play();
                    //MessageBox.Show("Play Sound");
                }
            }
            catch (Exception ex)
            {
                //MessageBox.Show("OnTimer(): " + ex.Message);
            }
        }

        public Stopwatch Stoper
        {
            get
            {
                return stopwatch;
            }
        }

        public DateTime Date
        {
            get
            {
                DateTime date = this.StandardTime.Add(CurrentDateSetting);
                return date;
            }

            set
            {
            }
        }

        public DateTime StandardTime
        {
            get
            {
                //docelowo w tym miejscu sterowanie trybem 12h/24h

                //CurrentTimeSetting = TimeSpan.FromMinutes(60);
                DateTime ClockTime = DateTime.Now.Add(CurrentTimeSetting);

                if (ClockTime.Hour < 12)
                {
                    CurrentTimeModeAM = true;
                }
                else
                {
                    CurrentTimeModeAM = false;
                }

                if (!CurrentTimeMode24h && !(ClockTime.Hour >= 0 && ClockTime.Hour <= 12))
                {
                    TimeSpan ampm = TimeSpan.FromHours(-12);
                    ClockTime = ClockTime.Add(ampm);
                    //MessageBox.Show("AM PM");
                }

                return ClockTime; //DateTime.Now + CurrentTimeSetting + (new TimeSpan(-2, 0, 0));
                
                // DateTime temp = DateTime.Now;
                // temp = temp.AddHours(3);
                // return temp;
                // this.richTextBoxDigitalClock.Text = DateTime.Now.ToLongTimeString(); // ToShortTimeString();
                // await Task.Delay(1000);
                

            }

            set
            {
            }
        }

        public DateTime T2
        {
            get
            {
                DateTime t2 = DateTime.Now.Add(CurrentT2Setting);
                return t2;
            }

            set
            {
            }
        }

        public DisplayFormat DisplayFormat
        {
            get
            {
                throw new System.NotImplementedException();
            }

            set
            {
            }
        }

        public DateFormat DateFormat
        {
            get
            {
                throw new System.NotImplementedException();
            }

            set
            {
            }
        }

        public AlarmStatus AlarmStatus
        {
            get
            {
                throw new System.NotImplementedException();
            }

            set
            {
            }
        }

        public void ChangeDate()
        {
            throw new NotImplementedException();
        }

        public void ChangeTime()
        {
            throw new System.NotImplementedException();
        }

        public void PlayChangeSound()
        {
            throw new System.NotImplementedException();
        }

        public void ChangeSettings()
        {
            throw new System.NotImplementedException();
        }
    }
}